# OpenShift Etherpad Template

This template is using a deprecated version of mysql image. Run the following command to import the image:

    oc import-image mysql-57-rhel7 --from=registry.redhat.io/rhscl/mysql-57-rhel7 -n openshift --confirm


To install Etherpad

    oc create -f etherpad-template.yml
    oc new-app --template=etherpad-persistent-mysql
